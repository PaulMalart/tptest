import { Grommet, Box, Text, Grid } from 'grommet';
import React, { Component } from 'react'
import themeFile from './features/utils/theme';
import dataFile from './features/utils/data.json';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import { Carte } from './features/Components/Card/Carte';
import Count from './features/Components/Count/Count';
import { About } from './features/Components/About/About'
import { Name } from './features/Components/Name/Name'
import { Home } from './features/Components/Home/Home'
import { Navbar } from './features/Navigation/Navbar'

export class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Grommet theme={themeFile} full>
        <Box direction="column" gap="large" margin="medium">
          <Router>
            <Navbar />
            <Switch>
              <Route exact path="/About" component={About}>
                <About />
              </Route>
              <Route exact path="/Name" component={Name}>
                <Name />
              </Route>
              <Route exact path="/Home" component={Home}>
                <Home />
              </Route>
            </Switch>
          </Router>
          <Box direction="row" justify="around">
            {
              dataFile.map(obj => {
                return (
                  <Carte age={obj.age} name={obj.name} />
                )
              })
            }
          </Box>
          <Count />
        </Box>
      </Grommet>
    )
  }
}

export default App

