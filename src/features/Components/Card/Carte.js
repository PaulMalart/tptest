import React, { Component } from 'react'
import { Card, CardBody, CardFooter, Button, CardHeader, Icons } from 'grommet'
import { Favorite, ShareOption } from 'grommet-icons';

export class Carte extends Component {
    render() {
        return (
            <Card height="small" width="small" background="light-1">
                <CardHeader pad="medium">{this.props.name} </CardHeader>
                <CardBody pad="medium">{this.props.age} Perches</CardBody>
                <CardFooter pad={{ horizontal: "small" }} background="light-2">
                    <Button
                        icon={<Favorite color="red" />}
                        hoverIndicator
                    />
                    <Button icon={<ShareOption color="plain" />} hoverIndicator />
                </CardFooter>
            </Card>
        )
    }
}

export default Card
