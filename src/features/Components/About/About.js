import React, { Component } from 'react'
import { connect } from 'react-redux'

export const About = () => {
    return (
        <div>
            <p>This is the About page</p>
        </div>
    )
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(About)
