import React from 'react'
import { Box, Text, Button } from 'grommet';

export default function Count() {
    const [count, setCount] = React.useState(0);
    React.useEffect(() => {
        setCount(3);
    }, []);
    return (
        <div>
            <Text>ouaais ouaais</Text>
            <Text>{count}</Text>
            <Button label="+1" onClick={() => setCount(count + 1)}></Button>
        </div>
    )
}
