import React, { Component, lazy } from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import { About } from '../Components/About/About'
import { Name } from '../Components/Name/Name'
import { Home } from '../Components/Home/Home'
// const Home = lazy(() => import("../Home/Home"));
// const About = lazy(() => import("../About/About"));
// // const Name = lazy(() => import("../Name/Name"));

//MUI IMPORTS
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

export class Navbar extends Component {
    render() {
        return (
            <AppBar position="fixed">
                <ToolBar>
                    <Button color="inherit" component={Link} to="/Home">Home</Button>
                    <Button color="inherit" component={Link} to="/About">About</Button>
                    <Button color="inherit" component={Link} to="/Name">Name</Button>
                </ToolBar>
            </AppBar>

        )
    }
}

export default Navbar
